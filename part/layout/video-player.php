<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly
$title     = get_sub_field( 'title' );
$pre_title = get_sub_field( 'pre-title' );
$choice    = get_sub_field( 'hero-or-slider' );
if ( $choice === 'hero' ): ?>
	<?php
	$hide_video_player = get_sub_field( 'hide-video-player' );
	?>
    <section class="video-player hero-setting <?php echo( $hide_video_player[0] === 'true' ? 'hide' : 'visible' ) ?>">
        <h2><?php echo $title ?></h2>
        <h3><?php echo $pre_title ?></h3>
		<?php
		$embed_link              = get_sub_field( 'embed-link' );
		$iframe_link             = get_sub_field( 'iframe-link' );
		$choice_of_mode          = get_sub_field( 'embed-or-iframe' );
		$choice_of_player        = get_sub_field( 'youtube-or-vimeo' );
		$controll_of_for_youtube = get_sub_field( 'turn-off-control-youtube' );
		?>
		<?php if ( $choice_of_mode === 'iframe' ): ?>
			<?php if ( $choice_of_player === 'youtube' ): ?>
                <div class="content youtube iframe">
                    <iframe src="<?php echo( ! empty( $iframe_link ) ? $iframe_link : 'https://www.youtube.com/embed/IVx6ZlksMJw' ) ?>?rel=0&amp;controls=<?php echo( $controll_of_for_youtube === true ? '0' : '1' ) ?>&amp;showinfo=0"
                            allowfullscreen frameborder="0"></iframe>
                </div>
			<?php endif; ?>
			<?php if ( $choice_of_player === 'vimeo' ): ?>
                <div class="content vimeo iframe">
                    <iframe src="<?php echo( ! empty( $iframe_link ) ? $iframe_link : 'https://player.vimeo.com/video/66966424' ) ?>?color=ff9933&byline=0&portrait=0"
                            frameborder="0" webkitallowfullscreen mozallowfullscreen
                            allowfullscreen></iframe>
                </div>
			<?php endif; ?>
		<?php endif; ?>
		<?php if ( $choice_of_mode === 'embed' ): ?>
			<?php if ( $choice_of_player === 'youtube' ): ?>
				<?php if ( ! empty( $embed_link ) ): ?>
                    <div class="content youtube embed">
						<?php echo $embed_link; ?>
                    </div>
				<?php endif; ?>
			<?php endif ?>
			<?php if ( $choice_of_player === 'vimeo' ): ?>
				<?php if ( ! empty( $embed_link ) ): ?>
                    <div class="content vimeo embed">
						<?php echo $embed_link; ?>
                    </div>
				<?php endif; ?>
			<?php endif; ?>
		<?php endif; ?>
    </section>
<?php elseif ( $choice === 'slider' ): ?>
    <section class="video-player slider-setting">
        <h2><?php echo $title ?></h2>
        <h3><?php echo $pre_title ?></h3>
		<?php

		?>
		<?php if ( have_rows( 'slider-player-settings' ) ) : ?>

            <div class="slider">
				<?php while ( have_rows( 'slider-player-settings' ) ) :
					the_row();
					$hide_video              = get_sub_field( 'hide-video' );
					$embed_link              = get_sub_field( 'embed-link' );
					$iframe_link             = get_sub_field( 'iframe-link' );
					$choice_of_mode          = get_sub_field( 'embed-or-iframe' );
					$choice_of_player        = get_sub_field( 'youtube-or-vimeo' );
					$controll_for_youtube = get_sub_field( 'turn-off-control-youtube' );

					/**
					 * START SLIDER
					 */
					?>
					<?php if ( $choice_of_mode === 'iframe' ): ?>
					<?php if ( $choice_of_player === 'youtube' ): ?>
                        <div class="content youtube iframe <?php echo( $hide_video[0] === 'true' ? 'hide' : 'visible' ) ?>">
                            <iframe
                                    src="<?php echo( ! empty( $iframe_link ) ? $iframe_link : 'https://www.youtube.com/embed/IVx6ZlksMJw' ) ?>?rel=0&amp;controls=<?php echo( $controll_for_youtube === true ? '0' : '1' ) ?>&amp;showinfo=0"
                                    allowfullscreen frameborder="0"></iframe>
                        </div>
					<?php endif; ?>
					<?php if ( $choice_of_player === 'vimeo' ): ?>
                        <div class="content vimeo iframe <?php echo( $hide_video[0] === 'true' ? 'hide' : 'visible' ) ?>">
                            <iframe src="<?php echo( ! empty( $iframe_link ) ? $iframe_link : 'https://player.vimeo.com/video/66966424' ) ?>?color=ff9933&byline=0&portrait=0"
                                    frameborder="0" webkitallowfullscreen mozallowfullscreen
                                    allowfullscreen></iframe>
                        </div>
					<?php endif; ?>
				<?php endif; ?>

					<?php if ( $choice_of_mode === 'embed' ): ?>
					<?php if ( $choice_of_player === 'youtube' ): ?>
						<?php if ( ! empty( $embed_link ) ): ?>
                            <div class="content youtube embed <?php echo( $hide_video[0] === 'true' ? 'hide' : 'visible' ) ?>">
								<?php echo $embed_link; ?>
                            </div>
						<?php endif; ?>
					<?php endif ?>
					<?php if ( $choice_of_player === 'vimeo' ): ?>
						<?php if ( ! empty( $embed_link ) ): ?>
                            <div class="content vimeo embed <?php echo( $hide_video[0] === 'true' ? 'hide' : 'visible' ) ?>">
								<?php echo $embed_link; ?>
                            </div>
						<?php endif; ?>
					<?php endif; ?>
				<?php endif; ?>
				<?php endwhile; ?>
            </div>
		<?php endif; ?>
    </section>
<?php endif; ?>