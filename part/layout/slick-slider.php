<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly
?>

<?php if ( have_rows( 'content' ) ) : ?>

    <section class="slick-slider">
        <div class="slider">
			<?php while ( have_rows( 'content' ) ) :
				the_row(); ?>
				<?php
				/**
				 * Slider Settings
				 */
				$image       = get_sub_field( 'image' );
				$pre_title   = get_sub_field( 'pre-title' );
				$title       = get_sub_field( 'title' );
				$hide_slider = get_sub_field( 'hide-slider' );
				?>
                <div class="jumbotron slick <?php echo( $hide_slider[0] === 'true' ? 'hide' : 'visible' ) ?>"
                     style="background-image: url(<?php echo $image['url'] ?>)">
                    <div class="content">
                        <h3><?php echo $title ?></h3>
                        <h2><?php echo $pre_title ?></h2>
                    </div>
                </div>
			<?php endwhile ?>
        </div>
    </section>
<?php endif ?>
