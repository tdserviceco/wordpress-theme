<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly
$hide_footer = get_sub_field( 'hide-footer' );
?>
<footer vertilize-container <?php echo( $hide_footer[0] === 'true' ? 'hide' : 'visible' ) ?>">
    <div>
		<?php
		/**
		 * COLOUMN 1
		 */
		?>
        <div class="col-sm-4" vertilize>
			<?php
			$choice = get_sub_field( 'text-or-image' );
			if ( $choice === 'image' ):
				$image = get_sub_field( 'image' );
				?>
                <img src="<?php echo $image['url'] ?>" alt="<?php echo $image['url'] ?>" class="logo">
			<?php endif;
			if ( $choice === 'logo-text' ):
				$logo_text = get_sub_field( 'logo-text' );
				?>
                <h3><?php
					if ( ! empty( $logo_text ) ):
						echo $logo_text;
					endif;
					?>
                </h3>
			<?php endif ?>
        </div>
		<?php
		/**
		 * COLOUMN 2
		 */
		?>
        <div class="col-sm-4" vertilize>
			<?php if ( have_rows( 'contact-area' ) ) : ?>
				<?php while ( have_rows( 'contact-area' ) ) :
					the_row(); ?>
					<?php
					$text_box = get_sub_field( 'text-box' );
					?>
                    <div class="content contact-area">
						<?php echo $text_box ?>
                    </div>
				<?php endwhile ?>
			<?php endif ?>
        </div>
		<?php
		/**
		 * COLOUMN 3
		 */
		?>
        <div class="col-sm-4" vertilize>
			<?php if ( have_rows( 'instagram-settings' ) ) : ?>
				<?php while ( have_rows( 'instagram-settings' ) ) :
					the_row(); ?>
					<?php
					$icon         = get_sub_field( 'icon' );
					$url          = get_sub_field( 'url' );
					$text_to_link = get_sub_field( 'text-to-link' );
					?>
                    <div class="content social-media instagram">
                        <span>
                            <a href="<?php echo $url ?>">
                                <?php echo $text_to_link ?>
                                <img src="<?php echo $icon['url'] ?>">
                            </a>
                        </span>
                    </div>
				<?php endwhile ?>
			<?php endif ?>
			<?php if ( have_rows( 'facebook-settings' ) ) : ?>
				<?php while ( have_rows( 'instagram-settings' ) ) :
					the_row(); ?>
					<?php
					$icon         = get_sub_field( 'icon' );
					$url          = get_sub_field( 'url' );
					$text_to_link = get_sub_field( 'text-to-link' );
					?>
                    <div class="content social-media twitter">
                        <span>
                            <a href="<?php echo $url ?>">
                                <?php echo $text_to_link ?>
                                <img src="<?php echo $icon['url'] ?>">
                            </a>
                        </span>
                    </div>
				<?php endwhile ?>
			<?php endif ?>
			<?php if ( have_rows( 'twitter-settings' ) ) : ?>
				<?php while ( have_rows( 'twitter-settings' ) ) :
					the_row(); ?>
					<?php
					$icon         = get_sub_field( 'icon' );
					$url          = get_sub_field( 'url' );
					$text_to_link = get_sub_field( 'text-to-link' );
					?>
                    <div class="content social-media twitter">
                        <span>
                            <a href="<?php echo $url ?>">
                                <?php echo $text_to_link ?>
                                <img src="<?php echo $icon['url'] ?>">
                            </a>
                        </span>
                    </div>
				<?php endwhile ?>
			<?php endif ?>
        </div>
    </div>
</footer>