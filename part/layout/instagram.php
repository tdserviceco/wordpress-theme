<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

// Construct the unique transient id
global $post;
if(get_field( 'token','option' ) === null || empty(get_field( 'token','option' ))):
	$sub_field     = '4124553885.1677ed0.4684468b83b84b19bc83903cfb4fc2ff';
	$token  = '4124553885.1677ed0.4684468b83b84b19bc83903cfb4fc2ff';
else :
	$sub_field     = get_field( 'token','option' );
	$token  = get_field( 'token','option' );
endif;

$parent_layout = $sub_field;
$transient_id  = "$parent_layout-$post->ID-flexible_elements_order";

// Settings
$count = ( ! empty( get_sub_field( 'count' ) ) ) ? get_sub_field( 'count' ) : 12;

$title        = get_sub_field( 'title' );
$text         = get_sub_field( 'text' );
$link_text    = get_sub_field( 'button-text' );
$link_url     = get_sub_field( 'link-url' );
$hide_section = get_sub_field( 'hide-section' );

// Settings used for comparing with transient
$settings = array(
	'token' => $token,
	'count' => $count
);

$transient = get_transient( $transient_id );

if ( ! empty( $transient ) && ! empty( $transient['settings'] ) && $transient['settings'] === $settings ) {
	$response = $transient['data'];
} else {

	// Get media from user account
	$url = "https://api.instagram.com/v1/users/self/media/recent/?access_token=$token&count=$count";

	$response = wp_remote_get( $url );
	$response = ( 200 === wp_remote_retrieve_response_code( $response ) ) ? json_decode( wp_remote_retrieve_body( $response ), true ) : '';

	$transient = array(
		'settings' => $settings,
		'data'     => $response
	);

	// Save transient
	set_transient( $transient_id, $transient, HOUR_IN_SECONDS );
}

?>

<?php if ( ! empty( $token ) && ! empty( $response ) ) : ?>

    <section class="instagram section-spacing <?php echo( $hide_section[0] === 'true' ? 'hide' : 'visible' ) ?>">

        <div class="container">

            <div class="tittle-desc">
				<?php if ( ! empty( $title ) ) : ?>
                    <h2><?php echo $title; ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $text ) ) : ?>
                    <p><?php echo $text; ?></p>
				<?php endif; ?>
            </div>

            <div class="layout-grid">
				<?php foreach ( $response['data'] as $instagram ) : ?>

                    <div class="instagram-post">
                        <a href="<?php echo $instagram['link'] ?>" target="_blank">
                            <img alt="<?php echo $instagram['caption']['text'] ?>"
                                 src="<?php echo $instagram['images']['standard_resolution']['url'] ?>"
                                 width="<?php echo $instagram['images']['standard_resolution']['width'] ?>"
                                 height="<?php echo $instagram['images']['standard_resolution']['height'] ?>"/>
                        </a>

                    </div>

				<?php endforeach; ?>
            </div>

			<?php if ( ! empty( $link_text ) && ! empty( $link_url ) ) : ?>
                <div class="col-md-12 section-button">
                    <a href="<?php echo esc_url( $link_url ) ?>" target="_blank"><?php echo $link_text ?></a>
                </div>
			<?php endif ?>
        </div>

    </section>
<?php endif ?>