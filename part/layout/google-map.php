<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly
?>
<section class="google-map <?php echo ($hide_section[0] === 'true' ? 'hide' : 'visible')?>">
    <?php
    $location = get_sub_field('map');
    $adress_googlemap = get_sub_field('adress-googlemap');
    $hide_section = get_sub_field('hide-section');
    if( !empty($location) ):
    ?>
    <div class="acf-map">
        <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"><?php echo $adress_googlemap?></div>
    </div>
    <?php endif ?>
</section>