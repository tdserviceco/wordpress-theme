<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly
?>
<?php
$image        = get_sub_field( 'image' );
$pre_title    = get_sub_field( 'pre-title' );
$title        = get_sub_field( 'title' );
$hide_section = get_sub_field( 'hide-section' );
$bg_color     = get_sub_field( 'background-color' );
?>
<section class="hero jumbotron <?php echo ($hide_section[0] === 'true' ? 'hide' : 'visible')?>" style="background: <?php echo $bg_color ?> url(<?php echo $image['url'] ?>)">
    <div class="content">

        <h3><?php
			if ( ! empty( $pre_title ) ):
				echo $pre_title;
			endif; ?>
        </h3>
        <h2>
			<?php
			if ( ! empty( $title ) ):
				echo $title;
			endif; ?>
        </h2>
    </div>
</section>