<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly
?>

<?php
$choice = get_sub_field( 'choice' );
if ( $choice === 'wysiwyg' ):?>

    <section class="text-section" >
		<?php
		// check if the repeater field has rows of data
		if ( have_rows( 'wysiwyg-editor-settings' ) ):

			// loop through the rows of data
			while ( have_rows( 'wysiwyg-editor-settings' ) ) : the_row(); ?>
				<?php
				// display a sub field value
				$hide_field     = get_sub_field( 'hide-field' );
				$wysiwyg_editor = get_sub_field( 'wysiwyg' );
				?>
                <div class="content container <?php echo( $hide_field[0] === 'true' ? 'hide' : 'visible' ) ?>">
					<?php echo $wysiwyg_editor; ?>
                </div>
				<?php
			endwhile;
		endif
		?>
    </section>
	<?php
endif;
if ( $choice === 'custom' ):?>
    <section class="text-section">
		<?php
		// check if the repeater field has rows of data
		if ( have_rows( 'custom-editor-settings' ) ):

			// loop through the rows of data
			while ( have_rows( 'custom-editor-settings' ) ) : the_row();

				// display a sub field value
				$hide_field = get_sub_field( 'hide-field' );
				$image      = get_sub_field( 'image' );
				$choice     = get_sub_field( 'image-position' );
				$title      = get_sub_field( 'title' );
				$text_area  = get_sub_field( 'text-area' );
				if ( $choice === 'left' ):?>
                    <div class="content container left <?php echo( $hide_field[0] === 'true' ? 'hide' : 'visible' ) ?>">
                        <div class="col-sm-12">
							<?php if ( ! empty( $image ) ): ?>
                                <img class="col-sm-4" src="<?php echo $image['url'] ?>"
                                     alt="<?php echo $image['alt'] ?>">
							<?php endif ?>
                            <div class="col-sm-8">
                                <h3><?php echo $title ?></h3>
                                <p><?php echo $text_area ?></p>
                            </div>
                        </div>
                    </div>
					<?php
				endif;
				if ( $choice === 'middle' ):?>
                    <div class="content container middle <?php echo( $hide_field[0] === 'true' ? 'hide' : 'visible' ) ?>">
						<?php if ( ! empty( $image ) ): ?>
                            <img class="col-sm-12" src="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>">
						<?php endif ?>
                        <h3 class="col-sm-12"><?php echo $title ?></h3>
                        <p class="col-sm-12"><?php echo $text_area ?></p>
                    </div>
					<?php
				endif;
				if ( $choice === 'right' ): ?>
                    <div class="content container right <?php echo( $hide_field[0] === 'true' ? 'hide' : 'visible' ) ?>">
                        <div class="col-sm-12">
							<?php if ( ! empty( $image ) ): ?>
                                <img class="col-sm-4" src="<?php echo $image['url'] ?>"
                                     alt="<?php echo $image['alt'] ?>">
							<?php endif ?>
                            <div class="col-sm-8">
                                <h3><?php echo $title ?></h3>
                                <p><?php echo $text_area ?></p>
                            </div>
                        </div>
                    </div>
				<?php endif; ?>
			<?php endwhile;
		endif
		?>
    </section>
<?php endif ?>