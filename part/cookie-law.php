<?php
/**
 * Cookie Law settings
 */

$background_color = get_field( 'background-color', 'option' );
$display_text     = get_field( 'display-text', 'option' );
$url              = get_field( 'url', 'option' );
$submit_text      = get_field( 'submit-button-text', 'option' );
$link_text        = get_field( 'link-text', 'option' );
$position         = get_field( 'display-position', 'option' );
if ( $position === 'header' ):
	?>
    <div id="cookie-overlay" class="header-position"
         style="background-color: <?php echo $background_color; ?>">
        <h2 id="cookie-header"><?php echo ( ! empty( $display_text ) ) ? __( $display_text, 'td' ) : __( 'Cookie Law', 'td' ) ?>
            <a href="<?php echo ( ! empty( $url ) ) ? $url : 'https://www.cookielaw.org/the-cookie-law/' ?>"><?php echo ( ! empty( $link_text ) ) ? $link_text : __( 'Link to site', 'td' ) ?></a>
        </h2>
        <input id="button" type="button"
               value="<?php echo ( ! empty( $submit_text ) ) ? $submit_text : __( 'I understand', 'td' ) ?>">
    </div>
<?php endif;

if ( $position === 'footer' ):
	?>
    <div id="cookie-overlay" class="footer-position"
         style="background-color: <?php echo $background_color; ?>">
        <h2 id="cookie-header"><?php echo ( ! empty( $display_text ) ) ? __( $display_text, 'td' ) : __( 'Cookie Law', 'td' ) ?>
            <a href="<?php echo ( ! empty( $url ) ) ? $url : 'https://www.cookielaw.org/the-cookie-law/' ?>"><?php echo ( ! empty( $link_text ) ) ? $link_text : __( 'Link to site', 'td' ) ?></a>
        </h2>
        <input id="button" type="button"
               value="<?php echo ( ! empty( $submit_text ) ) ? $submit_text : __( 'I understand', 'td' ) ?>">
    </div>
<?php endif; ?>
