</div>
<?php if ( get_field( 'display-position', 'option' ) === 'footer' && get_field( 'activate', 'option' ) === true ): ?>
	<?php get_template_part( '/part/cookie-law' ); ?>
<?php endif;
if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post() ?>
		<?php get_template_part( 'part/flexible-elements-footer' ); ?>
	<?php endwhile; ?>
<?php endif;
wp_footer();
?>
</body>
</html>
