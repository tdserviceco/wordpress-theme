<?php get_template_part( 'part/head' ) ?>
<body <?php body_class( '' ) ?> >
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#primary-navbar"
                        aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="<?php name::type_of_image( 'logo' ) ?>"></a>
            </div>

			<?php wp_nav_menu( array(
					'menu'            => 'header',
					'theme_location'  => 'header',
					'depth'           => 2,
					'container'       => 'div',
					'container_class' => 'collapse navbar-collapse',
					'container_id'    => 'primary-navbar',
					'menu_class'      => 'nav navbar-nav navbar-right navbar-lean underline',
					'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
					'walker'          => new wp_bootstrap_navwalker()
				)
			);
			?>
        </div>
    </nav>
<div class="page-wrapper">
<?php if ( get_field( 'display-position', 'option' ) === 'header' && get_field('activate', 'option') === true): ?>
	<?php get_template_part( '/part/cookie-law' ); ?>
<?php endif; ?>
