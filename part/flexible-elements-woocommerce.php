<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly
global $section_counter;

// check if ACF is available and the flexible content field has rows of data
if ( function_exists( 'have_rows' ) && have_rows( 'woocommerce' ) ) {

	// loop through the rows of data
	while ( have_rows( 'woocommerce' ) ) {

		the_row();
		$layout = get_row_layout();
		include( locate_template( 'part/woocommerce-layout/' . $layout . '.php' ) );
		$section_counter++;
	}

}