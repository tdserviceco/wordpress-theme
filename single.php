<?php if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly ?>

<?php include 'part/header.php' ?>

<?php if ( have_posts() ) : ?>

	<?php while ( have_posts() ) : the_post() ?>

		<?php get_template_part( 'part/flexible-elements' ) ?>

	<?php endwhile ?>

<?php endif ?>

<?php include 'part/footer.php' ?>