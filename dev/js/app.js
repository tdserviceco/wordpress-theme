console.log('Plugin loading....');
@import "../bower_components/jquery/dist/jquery.js";
@import "../bower_components/angular/angular.js";
@import "../bower_components/angular-route/angular-route.js";
@import "../bower_components/angular-resource/angular-resource.js"
@import "../bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.min.js";
@import "angularJS/angular-vertilize-master/angular-vertilize.js";
@import "../node_modules/angular-sidebarjs/dist/angular-sidebarjs.js";
@import "../bower_components/angular-sanitize/angular-sanitize.js";
@import "../bower_components/slick-carousel/slick/slick.js";
console.log('Finished loading all plugings');

//AngularJS
'use strict';
+function runThemeApp() {

    /**
     * To use some of the Dependency:
     *
     * Angular.Vertilize: https://github.com/Sixthdim/angular-vertilize
     */

    var appDependencies = [
        'ngRoute',
        'ngResource',
        'angular.vertilize',
        'ngSanitize'
    ];

    var themeApp = angular.module('angularApp',appDependencies)

    //Filters

    //Directives

}();

//Jquery functions
var $ = jQuery;
$(document).ready(function(){

    console.log('Loading jQuery Scripts:');
    
    @import "functions/_slick-slider.js";
    @import "functions/_google-maps.js";
    @import "functions/_js.cookie.js";
    @import "functions/_cookie-setup.js";

    console.log('jQuery scripts fully loaded');
})
