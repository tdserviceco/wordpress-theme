if( Cookies.get( 'cookie-accept' ) ) {
    $( '#cookie-overlay' ).css( 'display', 'none' );
}

$( 'input[type=button]#button' ).on( 'click', function( e ) {
    e.preventDefault();
    Cookies.set( 'cookie-accept', '1' );
    $( '#cookie-overlay' ).css( 'display', 'none' );
} );
