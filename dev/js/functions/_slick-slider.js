console.log( 'Slick Js loaded' );

/**
 * SLIDER SECTION
 */

if( sliderSettings.transitionSpeedSlider.length === 0 || sliderSettings.transitionSpeedSlider === null) {
    sliderSettings.transitionSpeedSlider = 300;
}

if( sliderSettings.amount.length === 0 ) {
    sliderSettings.amount = 1;
}

if( sliderSettings.autoPlaySpeedSlider.length === 0 || sliderSettings.autoPlaySpeedSlider === null) {
    sliderSettings.autoPlaySpeedSlider = 3000;
}

if( sliderSettings.activateAutoPlaySlider === 1 ) {
    sliderSettings.activateAutoPlaySlider = true;
}

if( sliderSettings.arrowBoolean.length === 1 ) {
    sliderSettings.arrowBoolean = true;
}

if( sliderSettings.dotsBoolean.length === 1 ) {
    sliderSettings.dotsBoolean = true;
}

$( 'section.slick-slider > .slider' ).slick( {

    responsive     : [
        {
            breakpoint : 920,
            settings   : {
                slidesToShow   : 1,
                slidesToScroll : 1
            }
        }
    ],
    slidesToShow   : sliderSettings.amount,
    slidesToScroll : 1,
    centerMode     : false,
    centerPadding  : '0',
    autoplay       : sliderSettings.activateAutoPlaySlider,
    autoplaySpeed  : sliderSettings.autoPlaySpeedSlider,
    speed          : sliderSettings.transitionSpeedSlider,
    arrows         : sliderSettings.arrowBoolean,
    dots           : sliderSettings.dotsBoolean,
    prevArrow      : '<button class="slick-arrow slick-prev"><i class="fa fa-angle-left"></i><span class="sr-only">' + translation.previous_language + '</span></button>',
    nextArrow      : '<button class="slick-arrow slick-next"><i class="fa fa-angle-right"></i><span class="sr-only">' + translation.next_language + '</span></button>'

} );


/**
 * TEASER SECTION
 */


if( teaserSettings.transitionSpeedTeaser === null || teaserSettings.transitionSpeedTeaser.length === 0  ) {
    teaserSettings.transitionSpeedTeaser = 300;
}

if( teaserSettings.activateAutoPlayTeaser === 1 ) {
    teaserSettings.activateAutoPlayTeaser = true;
}

if( teaserSettings.autoPlaySpeedTeaser === null || teaserSettings.autoPlaySpeedTeaser.length === 0 ) {
    teaserSettings.autoPlaySpeedTeaser = 3000;
}

if( teaserSettings.amount.length === 0 ) {
    teaserSettings.amount = 3;
}

if( teaserSettings.arrowBoolean.length === 1 ) {
    teaserSettings.arrowBoolean = true;
}

if( teaserSettings.dotsBoolean.length === 1 ) {
    teaserSettings.dotsBoolean = true;
}

$( 'section.teasers > .slider' ).slick( {

    responsive     : [
        {
            breakpoint : 920,
            settings   : {
                slidesToShow   : 1,
                slidesToScroll : 1
            }
        }
    ],
    slidesToShow   : teaserSettings.amount,
    slidesToScroll : 1,
    centerMode     : false,
    centerPadding  : '0',
    autoplay       : teaserSettings.activateAutoPlayTeaser,
    autoplaySpeed  : teaserSettings.autoPlaySpeedTeaser,
    speed          : teaserSettings.transitionSpeedTeaser,
    arrows         : teaserSettings.arrowBoolean,
    dots           : teaserSettings.dotsBoolean,
    prevArrow      : '<button class="slick-arrow slick-prev"><i class="fa fa-angle-left"></i><span class="sr-only">' + translation.previous_language + '</span></button>',
    nextArrow      : '<button class="slick-arrow slick-next"><i class="fa fa-angle-right"></i><span class="sr-only">' + translation.next_language + '</span></button>'

} );


/**
 * VIDEO PLAYER SLIDER
 */


$( 'section.video-player > .slider' ).slick( {

    responsive     : [
        {
            breakpoint : 920,
            settings   : {
                slidesToShow   : 1,
                slidesToScroll : 1
            }
        }
    ],
    slidesToShow   : 3,
    slidesToScroll : 1,
    centerMode     : false,
    centerPadding  : '0',
    autoplay       : false,
    autoplaySpeed  : 5000,
    speed          : 500,
    arrows         : false,
    dots           : false,
    prevArrow      : '<button class="slick-arrow slick-prev"><i class="fa fa-angle-left"></i><span class="sr-only">' + translation.previous_language + '</span></button>',
    nextArrow      : '<button class="slick-arrow slick-next"><i class="fa fa-angle-right"></i><span class="sr-only">' + translation.next_language + '</span></button>'

} );


