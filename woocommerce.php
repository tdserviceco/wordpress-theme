<?php if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly ?>

<?php include 'part/header.php' ?>

	<?php woocommerce_content(); ?>
	<?php get_template_part( 'part/flexible-elements-woocommerce' ) ?>

<?php include 'part/footer.php' ?>